#!/usr/bin/env python
# coding: utf-8

# In[104]:


def knapSack(maxWeight,itemList,n):
    #inistializing kanpSackMatrix of size n+1 x maxWeight+1
    knapSackMatrix = [[0 for y in range(maxWeight+1)] for y in range(n+1)]
    
    for i in range(n+1):
        for currentWeight in range(maxWeight+1):
            #if no item is selected and current Weight allowed is 0 
            if i==0 or currentWeight==0:
                knapSackMatrix[i][currentWeight]=0
            #if item weight is smaller or equal than current allowed weight
            elif itemList[i-1][1]<=currentWeight:
                #find max profit if we add or not add the particular item
                knapSackMatrix[i][currentWeight] = max(itemList[i-1][2]+knapSackMatrix[i-1][currentWeight-itemList[i-1][1]],knapSackMatrix[i-1][currentWeight])
            #if item weight is greater than current allowed weight
            else:
                #get max profit if we do not add the given item
                knapSackMatrix[i][currentWeight] = knapSackMatrix[i-1][currentWeight]
    #print(knapSackMatrix)
    #storing maximum profit
    maxProfit= knapSackMatrix[n][maxWeight]
    #finding list of paints that will give maximum profit
    resList= findResultList(maxProfit,maxWeight, n, knapSackMatrix, itemList)
    #finding remaining capacity
    remainingCapacity = findRemainingCapacity(maxWeight,itemList, resList)
    return resList, maxProfit , remainingCapacity

#Func To find resultList from the KnapSack Matrix
def findResultList(profit,capacity, n, knapSackMatrix, itemList):
    resList=[]
    for i in range(n,0,-1):
        if(profit<=0):
            break
        #if profit doesn't increased from adding given paint then continue
        elif(profit == knapSackMatrix[i-1][capacity]):
            continue
        #if profit get increased by adding the given paint add item in the list
        else:
            resList.append(itemList[i-1][0])
            #reducing item profit from final profit 
            profit-= itemList[i-1][2]
            #reducing item capacity from final capacity
            capacity-=itemList[i-1][1]
    resList.sort()
    return resList

#Func to find remaining capacity
def findRemainingCapacity(capacity, ItemList, resultList):
    #for each item in itemlist
    for item in ItemList:
        #if that item number exist in resultList 
        if item[0] in resultList:
            #minus weight from remaining cpacity
            capacity -= item[1]
    return capacity

#Func to read input file and create list from the given data
def readInputfile(inputfile):
    resultList = []
    #open input file with read mode
    inputFile = open(inputfile, "r")
    #read lines and create list of lines
    inputs = inputFile.readlines()
    #split each line into list, converting them into integer list
    #resultList[i][0] represent paint number
    #resultList[i][1] represent Capacity required from given paint 
    #resultList[i][2] represent Profit from given paint
    for line in inputs:
        lineList = line.strip().split('/')
        resultList.append([int(item.strip()) for item in lineList])  
    inputFile.close()
    # sorting list by capacity value
    resultList.sort(key = lambda x: x[1])
    return resultList

#Func to write output in output file 
def writeOutput(resultList,profit,remainingCapacity):
    outputFile = open("outputPS16.txt", "w")
    # Printing paint numbers to be funded in output file
    outputFile.write("The paints that should be funded: ")
    outputFile.write(",".join(str(i) for i in resultList))
    outputFile.write("\n")
    # Printing Total Profit in Out Put File
    outputFile.write("Total profit: ")
    outputFile.write(str(profit))
    outputFile.write("\n")
    # Printing Remaining Capacity in Output file
    outputFile.write("Capacity Remaining: ")
    outputFile.write(str(remainingCapacity))
    


# In[106]:


from pathlib import Path

if Path("InputPS16.txt").is_file():
    #read input file and creating graph according to the given input
    itemList = readInputfile("InputPS16.txt")
    #print(itemList)
    #max capacity that can be produced
    maxWeight =1000
    #number of paints in the list
    n= len(itemList)
    #find resultList, profit, remainingCapacity for given list of paints
    resultList, profit, remainingCapacity = knapSack(maxWeight,itemList,n)
    #writing the result in output file
    writeOutput(resultList,profit,remainingCapacity)
else:
    print("Input file is not available on the path given.")
print("done")


# In[ ]:




